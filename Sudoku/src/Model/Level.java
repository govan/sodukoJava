package Model;

/**
 * Enum für den Schwierigkeitsgrad des Spiels
 * EASY     :   einfach (viele Felder vorgegeben)
 * MIDDLE   :   mittel (weniger Felder vorgegeben als bei EASY)
 * HARD     :   schwer (wenige Felder vorgegeben)
 * @author Florian Kallabinski
 * @version 1
 * @since 02.06.16
 */
public enum Level {
    EASY, MIDDLE, HARD
}
