package Model;

import Model.FieldInfo;
import Model.Level;
import Model.TNumberField;

import java.util.Random;

/**
 * Klasse zum Generieren eines Sudoku-Spiels
 *
 * @author Vanessa Suchomel
 * @version 1
 * @since 21.06.16
 */

public class SudokuGenerator {
    public static final int STANDARD_SIZE = 9;

    private TNumberField[][] field;
    private TNumberField[][] fieldFull;
    private int values[];
    private int num_box; //Anzahl der Boxen pro Reihe, Spalte
    private int value;
    private int x;
    private int y;

    /**
     * Setzt Zahlen je nach Schwieigkeitsgrad auf Null
     * @param   level   Level des Spiels
     * @return  TRUE    Spiel konnte generiert werden
     *          FALSE   Spiel konnte nicht generiert werden
     */
    private boolean generateLevel(Level level) {
        int numbersToRemove;
        int randomX;
        int randomY;

        switch (level) {
            case EASY:
                numbersToRemove = 30;
                break;
            case MIDDLE:
                numbersToRemove = 40;
                break;
            case HARD:
                numbersToRemove = 50;
                break;
            default:
                numbersToRemove = 0;
                break;
        }
        while (numbersToRemove != 0) {
            randomX = (int) (Math.random() * 9);
            randomY = (int) (Math.random() * 9);
            if (field[randomX][randomY].getValue() == 0) {
                continue;
            }
            field[randomX][randomY].setValue(0);
            field[randomX][randomY].setInfo(FieldInfo.NORMAL);
            numbersToRemove--;
        }
        return true;
    }

    /**
     * Erstellt ein neues, gültiges Spiel
     * @param   level   Level des Spiels
     * @return  TRUE    Spiel konnte generiert werden
     *          FALSE   Spiel konnte nicht generiert werden
     */
    public Boolean generateStandardSudoku(Level level) {
        int i, j;
        field = new TNumberField[STANDARD_SIZE][STANDARD_SIZE];
        fieldFull = new TNumberField[STANDARD_SIZE][STANDARD_SIZE];
        num_box = (int) Math.sqrt(STANDARD_SIZE); //Anzahl berechnen
        values = new int[STANDARD_SIZE];
        boolean fieldUncomplete = true;

        while (fieldUncomplete) {
            for (int count = 0; count < STANDARD_SIZE; count++) {
                values[count] = count + 1;
            }
            for (i = 0; i < STANDARD_SIZE; i++) {
                for (j = 0; j < STANDARD_SIZE; j++) {
                    field[i][j] = new TNumberField(0,FieldInfo.GIVEN);
                }
            }
            fieldUncomplete = !generate();
        }
        
        // wichtig neue TNumberFields zu erstellen, da sonst field und fieldFull auf die gleichen Referenzen zugreifen
        for (i = 0; i < STANDARD_SIZE; i++) {
            for (j = 0; j < STANDARD_SIZE; j++) {
                fieldFull[i][j] = new TNumberField(field[i][j]);
            }
        }

        generateLevel(level);
        return true;
    }

    /**
     * Gibt das Spielfeld zurück
     * @return  field
     */
    public TNumberField[][] getField() {
        return field;
    }

    /**
     * Gibt das volle Spielfeld zurück
     * @return  fieldFull
     */
    public TNumberField[][] getFieldFull() {
        return fieldFull;
    }

    /**
     * Gibt das Feld an der Stelle x, y zurück
     * @param x     x-Koordinate
     * @param y     y-Koordinate
     * @return  field[x][y]
     */
    public TNumberField getField(int x, int y) {
        return field[x][y];
    }

    /**
     * Besetzt das Feld mit einem Wert an Stelle x,y
     * @param value     Wert, der gesetzt werden soll
     * @param x         x-Koordinate
     * @param y         y-Koordinate
     */
    public void setField(int value, int x, int y) {
        field[x][y] = new TNumberField(value, FieldInfo.GIVEN);
    }

    /**
     * Setter für Sodukofeld
     * @param field zu setzendes Feld
     */
    public void setWholeField(TNumberField field[][]){
        this.field = field;
    }

    /**
     * Generiert ein Spielfeld
     * @return  TRUE    Spiel wurde generiert
     *          FALSE   Spiel wurde nicht generiert
     */
    private boolean generate() {
        int current = 0;
        for (int x = 0; x < STANDARD_SIZE; x++) {
            for (int y = 0; y < STANDARD_SIZE; y++) {
                mixValues();
                for (int count = 0; check(current, x, y); count++) {
                    if (count == 9) {

                        return false;
                    }
                    current = values[count];
                }
                setField(current, x, y);
            }
        }
        return true;
    }

    /**
     * Sucht zufällige Zahlen zwischen 1 und 9
     */
    private void mixValues() {
        int save, z;
        Random r = new Random();
        for (int count = 0; count < STANDARD_SIZE; count++) {
            z = 0;
            while (z == 0) {
                z = r.nextInt(STANDARD_SIZE);
            }
            save = values[count];
            values[count] = values[z];
            values[z] = save;
        }
    }

    /**
     * Schaut, ob die eingegebene Ziffer schon in Spalte, Reihe oder Kasten vorhanden ist
     * @param value     gesuchter Wert
     * @param x         x-Koordinate
     * @param y         y-Koordinate
     * @return  TRUE    Wert noch nicht vorhanden
     *          FALSE   Wert vorhanden
     */
    private boolean check(int value, int x, int y) {
        if (checkVertical(value, y)) {
            return true;
        }
        if (checkHorizontal(value, x)) {
            return true;
        }
        if (checkBox(value, x, y)) {
            return true;
        }
        return false;
    }

    /**
     * Schaut, ob die eingegebene Ziffer schon in der Spalte vorhanden ist
     * @param value     gesuchter Wert
     * @param y         y-Koordinate
     * @return  TRUE    Wert ist noch nicht vorhanden
     *          FALSE   Wert ist schon vorhanden
     */
    private boolean checkVertical(int value, int y) {
        for (int x = 0; x < STANDARD_SIZE; x++) {
            if (getField(x, y).getValue() == value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Schaut, ob die eingegebene Ziffer schon in der Zeile vorhanden ist
     * @param value     gesuchter Wert
     * @param x         x-Koordinate
     * @return  TRUE    Wert ist noch nicht vorhanden
     *          FALSE   Wert ist schon vorhanden
     */
    private boolean checkHorizontal(int value, int x) {
        for (int y = 0; y < STANDARD_SIZE; y++) {
            if (getField(x, y).getValue() == value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Schaut, ob die eingegebene Ziffer schon in der Spalte vorhanden ist
     * @param value     gesuchter Wert
     * @param x         x-Koordinate
     * @param y         y-Koordinate
     * @return  TRUE    Wert ist noch nicht vorhanden
     *          FALSE   Wert ist schon vorhanden
     */
    private boolean checkBox(int value, int x, int y) {
        int field_x = (int) (x / num_box) * num_box;
        int field_y = (int) (y / num_box) * num_box;

        for (x = field_x; x < field_x + num_box; x++) {
            for (y = field_y; y < field_y + num_box; y++) {
                if (value == getField(x, y).getValue()) {
                    return true;
                }
            }
        }
        return false;
    }

}
