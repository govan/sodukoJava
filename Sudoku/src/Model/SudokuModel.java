package Model;

import java.io.*;
import java.util.Date;

/**
 * Klasse für das Model eines Sudoku-Spiels
 *
 * @author Florian Kallabinski
 * @version 1
 * @since 03.06.16
 */
public class SudokuModel implements Serializable {
    private TNumberField[][] field;
    private TNumberField[][] fieldFull;
    private Date startTime;
    private long playTimeInSec;
    private String playerName;
    private Level level;

    /**
     * erstellt ein neues Spiel
     *
     * @param playerName Spielername
     * @param level      Schwierigkeitsgrad
     * @return  TRUE    Spiel konnte generiert werden
     *          FALSE   es ist etwas schief gelaufen
     */
    public Boolean newGame(String playerName, Level level) {
        SudokuGenerator sudokuGenerator = new SudokuGenerator();
        if (!sudokuGenerator.generateStandardSudoku(level))
            return false;
        this.level = level;
        this.field = sudokuGenerator.getField();
        this.fieldFull = sudokuGenerator.getFieldFull();
        this.playerName = playerName;

        this.startTime = new Date(System.currentTimeMillis());

        return true;
    }


    /**
     * überprüft, ob der Spieler das Feld richtig ausgefüllt hat
     *
     * @return  TRUE    richtig ausgefüllt
     *          FALSE   falsch ausgefüllt
     */
    public Boolean checkResult() {
        int i;
        int j;
        Boolean result;

        if ((field == null) || (fieldFull == null)) {
            // evtl. auch Meldung ausgeben und/oder Exception werfen
            return false;
        }

        result = true;
        for (i = 0; i < field.length; i++) {
            for (j = 0; j < field[i].length; j++) {
                if (field[i][j].getValue() != fieldFull[i][j].getValue()) {
                    field[i][j].setInfo(FieldInfo.WRONG);
                    result = false;
                // wurde ein vorher falsches Feld korrigiert, muss die Info angepasst werden
                } else if (field[i][j].getInfo().equals(FieldInfo.WRONG)) {
                    field[i][j].setInfo(FieldInfo.NORMAL);
                }
            }
        }
        return result;
    }

    /**
     * muss vom Controller aufgerufen werden, damit er das Spielfeld darstellen kann
     *
     * @return Spielfeld
     */
    public TNumberField[][] getField() {
        return field;
    }

    /**
     * wird vom Controller aufgerufen, wenn der Spieler den Wert eines Feldes verändert
     *
     * @param x Zeile des Felds
     * @param y Spalte des Felds
     * @param val Wert des Felds
     */
    public void setNumberField(int x, int y, TNumberField val) {
        if ((val != null) && (x < field.length) && (y < field[x].length)) {
            field[x][y] = val;
        }
    }

    /**
     * eine Möglichkeit das Spielfeld zu setzen
     * evtl. ganz nützlich für die Tests...
     *
     * @param field     lückenhaftes Spielfeld
     * @param fieldFull ausgefülltes Spielfeld
     */
    public void setField(TNumberField[][] field, TNumberField[][] fieldFull) {
        this.field = field;
        this.fieldFull = fieldFull;
    }

    /**
     * erstellt einen neuen Spielstand und speichert ihn im Verzeichnis "src/saves"
     *
     * @param saveName Name des Spielstands
     * @return  TRUE    Spielstand wurde erfolgreich gespeichert
     *          FALSE   Fehler beim Speichern
     */
    public Boolean saveGame(String saveName) {
        if (startTime != null) {
            playTimeInSec = (System.currentTimeMillis() - startTime.getTime()) / 1000;
        } else {
            playTimeInSec = 0;
        }
        SudokuSave sudSave = new SudokuSave();

        return sudSave.saveGame(this, saveName);
    }

    /**
     * Setter für die Spielzeit in Sekunden
     * @param playTimeInSec Spielzeit in Sekunden
     */
    public void setPlayTimeInSec(long playTimeInSec) {
        this.playTimeInSec = playTimeInSec;
    }

    /**
     * Getter für die Spielzeit in Sekunden
     * @return  Spielzeit in Sekunden
     */
    public long getPlayTimeInSec() {
        return playTimeInSec;
    }

    /**
     * Setter für die Zeit, zu der das Spiel gestartet wurde
     * @param startTime Startzeit
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter für die Zeit, zu der das Spiel gestartet wurde
     * @return  Startzeit
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Getter für den Schwierigkeitsgrad
     * @return  Schwierigkeitsgrad
     */
    public Level getLevel() {
        return level;
    }

    /**
     * Getter für den Spielernamen
     * @return  Spielername
     */
    public String getPlayerName() {
        return playerName;
    }

}
