package Model;

import java.io.Serializable;
import java.util.Date;

/**
 * Klasse, die einen Highscore-Eintrag repräsentiert
 *
 * ein highscore-Eintrag sieht so aus:
 * 1:Date eigentlicher Highscore (Spielzeit)
 * 2:Date Zeit des Highscores
 * 3:String playerName
 *
 * @author Florian Beyer
 * @version 1
 * @since 23.06.16
 */
public class HighscoreEntry implements Comparable, Serializable {
    private Long playTimeInSeconds;
    private Date entryTime;
    private String playerName;

    /**
     * Getter für die Spielzeit
     * @return  Long    Spielzeit
     */
    public Long getPlayTimeInSeconds() {
        return playTimeInSeconds;
    }

    /**
     * Setter für die Spielzeit
     * @param playTimeInSeconds Spielzeit
     */
    public void setPlayTimeInSeconds(Long playTimeInSeconds) {
        this.playTimeInSeconds = playTimeInSeconds;
    }

    /**
     * Getter für den Zeitpunkt des Erstellens des Highscore-Eintrags
     * @return  Date    Zeit des Highscore-Eintrags
     */
    public Date getEntryTime() {
        return entryTime;
    }

    /**
     * Setter für den Zeitpunkt des Erstellens des Highscore-Eintrags
     * @param entryTime Zeit des Highscore-Eintrags
     */
    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    /**
     * Getter für den Spielernamen
     * @return  String  Spielername
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * Setter für den Spielernamen
     * @param playerName    Spielername
     */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    /**
     * compareTo-Methode überschrieben, damit Highscore-Einträge richtig sortiert werden
     * @param o Highscore-Eintrag
     * @return  <0  aktueller Eintrag kleiner
     *          =0  Einträge gleich
     *          >0  anderer Eintrag kleiner
     */
    @Override
    public int compareTo(Object o) {
        if (!(o instanceof HighscoreEntry))
            return -1;
        HighscoreEntry other = (HighscoreEntry) o;
        if (other.getPlayTimeInSeconds() == null)
            return -1;
        if (playTimeInSeconds == null)
            return 1;
        // ansonsten können nicht zwei Einträge mit der gleichen Zeit existieren
        if (playTimeInSeconds.equals(other.getPlayTimeInSeconds()))
            return 1;
        return playTimeInSeconds.compareTo(other.getPlayTimeInSeconds());
    }
}
