package Model;

/**
 * Enum, der den Status eines Feldes darstellt
 * NORMAL   :   Feld war nicht vorbelegt und entweder noch nicht geprüft oder nach Prüfung korrekt
 * GIVEN    :   Feld war bereits vorgegeben
 * WRONG    :   Feld beinhaltet einen falschen Wert
 *
 * @author Florian Kallabinski
 * @version 1
 * @since 02.06.16
 */
public enum FieldInfo {
    NORMAL, GIVEN, WRONG
}
