package Model;

import Model.FieldInfo;

import java.io.Serializable;

/**
 * Klasse, die ein Feld eines Sudoku-Spiels repräsentiert
 * (aus Mangel an structs wird Klasse verwendet)
 *
 * beinhaltet:
 * - den vom Spieler eingegeben Wert
 * - bis zu zwei Notizen des Spielers
 * - einen Wert vom Typ FieldInfo, der den Status des Feldes repräsentiert
 *
 * @author Florian Kallabinski
 * @version 1
 * @since 02.06.16
 */
public class TNumberField implements Serializable {
    private int value;
    private int note1;
    private int note2;
    private FieldInfo info;

    /**
     * Copy-Konstruktor für TNumberField
     * wird beim generieren des Sudokus aufgerufen, damit field und fieldFull nicht auf die gleichen TNumberFields zugreifen
     * @param fieldToCopy
     */
    public TNumberField(TNumberField fieldToCopy) {
        if (fieldToCopy != null) {
            this.value = fieldToCopy.getValue();
            this.note1 = fieldToCopy.getNote1();
            this.note2 = fieldToCopy.getNote2();
            this.info = fieldToCopy.getInfo();
        }
    }

    /**
     * Standard-Konstruktor für TNumberField
     * wird für normale, nicht vorgegebene Felder aufgerufen
     */
    public TNumberField() {
        this(0, FieldInfo.NORMAL);
    }

    /**
     * Konstruktor mit Parameter für den Inhalt des Feldes
     * Status wird mit normal vorbelegt
     */
    public TNumberField(int value) {
        this(value, FieldInfo.NORMAL);
    }

    /**
     * Konstruktor für TNumberField
     * wird für vorgegebene Felder aufgerufen
     * @param value vorgegebener Wert
     * @param info Status des Feldes
     */
    public TNumberField(int value, FieldInfo info) {
        this.value = value;
        this.note1 = 0;
        this.note2 = 0;
        this.info = info;
    }

    /**
     * prüft, ob aktuelles und übergebenes Objekt gleich sind
     * (nicht ob sie die gleiche Referenz haben)
     * @param o Objekt, das auf Gleichheit überprüft werden soll
     * @return  TRUE    Objekte gleich
     *          FALSE   Objekte unterschiedlich
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TNumberField that = (TNumberField) o;

        if (value != that.value) return false;
        if (note1 != that.note1) return false;
        if (note2 != that.note2) return false;
        return info == that.info;

    }

    /**
     * gibt einen Hash-Code für das Objekt zurück
     * @return    Hash-Code
     */
    @Override
    public int hashCode() {
        int result = value;
        result = 31 * result + note1;
        result = 31 * result + note2;
        result = 31 * result + (info != null ? info.hashCode() : 0);
        return result;
    }

    /**
     * Setter für den Inhalt des Feldes
     * @param value Wert
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Setter zum Vormerken eines möglichen Wertes
     * @param note1 Notiz
     */
    public void setNote1(int note1) {
        this.note1 = note1;
    }

    /**
     * Setter zum Vormerken eines weiteren möglichen Wertes
     * @param note2 Notiz
     */
    public void setNote2(int note2) {
        this.note2 = note2;
    }

    /**
     * Setter zum Ändern des Status des Feldes
     * @param info Feld-Status
     */
    public void setInfo(FieldInfo info) {
        this.info = info;
    }



    /**
     * Getter für den Inhalt des Feldes
     * @return Feldinhalt
     */
    public int getValue() {
        return value;
    }

    /**
     * Getter für die erste Notiz
     * @return erster vorgemerkter Wert
     */
    public int getNote1() {
        return note1;
    }

    /**
     * Getter für die zweite Notiz
     * @return zweiter vorgemerkter Wert
     */
    public int getNote2() {
        return note2;
    }

    /**
     * Getter für den Status des Feldes
     * @return Feld-Status
     */
    public FieldInfo getInfo() {
        return info;
    }
}
