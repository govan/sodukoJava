package Model;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Klasse für das Speichern und Laden von bereits begonnenen Sudokus
 *
 * @author Florian Beyer
 * @version 1
 * @since 23.06.16
 */
public class SudokuSave {
    private final String saveDirectory;
    private final String saveFileExt;

    /**
     * Standard-Konstruktor für SudokuSave
     * initialisiert Pfad zum Speicherort und File-Extension für die Spielstände
     */
    public SudokuSave() {
        saveDirectory = "saves/";
        saveFileExt = ".ser";
    }

    /**
     * Speichert das übergebene Model Obj serialisiert in den saves Ordner ab
     *
     * @param model
     * @param saveName
     * @return  TRUE    Speichern erfolgreich
     *          FALSE   es ist etwas schief gelaufen
     */
    public Boolean saveGame(SudokuModel model, String saveName) {
        try {
            File f = new File(saveDirectory);
            if (!f.exists()) {
                // falls das angegebene Verzeichnis nicht existiert, wird es erstellt
                f.mkdirs();
            }
            FileOutputStream fileOut = new FileOutputStream(saveDirectory + saveName + saveFileExt);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(model);
            out.close();
            fileOut.close();
            //System.out.printf("Serialized data is saved in src/saves/" + saveName + ".ser");
        } catch (IOException i) {
            i.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * lädt ein gespeichertes Spiel
     *
     * @return  TRUE    Laden erfolgreich
     *          FALSE   es ist etwas schief gelaufen
     */
    public SudokuModel loadGame(SudokuModel model, String saveName) {
        File f = new File(saveDirectory + saveName + saveFileExt);

        if (f.exists()) {
            try {

                FileInputStream fileIn = new FileInputStream(f);
                ObjectInputStream in = new ObjectInputStream(fileIn);

                model = (SudokuModel) in.readObject();

                in.close();
                fileIn.close();
            } catch (InvalidClassException e){
                Alert alert = new Alert(Alert.AlertType.NONE, "Error!\nEs wurde versucht einen veralteten Spielstand zu laden! Diese sind nicht mehr unterstützt!", ButtonType.OK);
                alert.showAndWait();
            } catch (IOException i) {
                i.printStackTrace();
                return null;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            /*
            setzt die Startzeit um die vergangene Zeit zurück
             */
            model.setStartTime(new Date(System.currentTimeMillis() - model.getPlayTimeInSec() * 1000));

            return model;
        }
        return null;
    }


    /**
     * gibt eine Liste zurück, in der alle Einträge des Ordners vorhanden sind
     *
     * @param path
     * @return
     * @throws IOException
     */
    public List<Path> showSaves(Path path) throws IOException {
        List<Path> saveList = new ArrayList<>();

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path entry : stream) {

                /*if (Files.isDirectory(entry)) {
                    listFiles(entry); //für Unterordner auch noch
                }*/
                if (Files.isDirectory(entry)) {
                    continue;
                }
                Path test = entry.getFileName();
                saveList.add(test);
                //files.add(entry);
            }
        }

        return saveList;
    }
}
