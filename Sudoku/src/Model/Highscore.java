package Model;

import java.io.*;
import java.util.*;

/**
 * Klasse für die Verwaltung der Highscores
 * @author Florian Beyer
 * @version 1
 * @since 23.06.16
 */
public class Highscore implements Serializable {
    private final String saveFilename;
    private final String saveDirectory;
    private Set<HighscoreEntry> easySet = new TreeSet<>();
    private Set<HighscoreEntry> middleSet = new TreeSet<>();
    private Set<HighscoreEntry> hardSet = new TreeSet<>();

    /**
     * Standard-Konstruktor für Highscore
     * initialisiert Pfad zum Speicherort und den Dateinamen für die Highscores
     */
    public Highscore() {
        saveDirectory = "saves/highscore/";
        saveFilename = "Highscore.ser";
    }

    /**
     * fügt einen neuen Highscore-Eintrag zur jeweiligen Liste hinzu
     * @param playTimeInSeconds Spielzeit
     * @param playerName    Spielername
     * @param level Schwierigkeitsgrad
     */
    public void createEntry(long playTimeInSeconds, String playerName, Level level) {
        loadHighscoreList();

        HighscoreEntry entry = new HighscoreEntry();
        entry.setPlayTimeInSeconds(playTimeInSeconds);
        entry.setEntryTime(new Date(System.currentTimeMillis()));
        entry.setPlayerName(playerName);

        switch (level) {
            case EASY:
                easySet.add(entry);
                break;
            case MIDDLE:
                middleSet.add(entry);
                break;
            case HARD:
                hardSet.add(entry);
                break;
        }

        saveHighscoreList();
    }

    /**
     * gibt die jeweilige Highscoreliste zurück
     * @param level Schwierigkeitsgrad
     * @return  Set<HighscoreEntry> Highscoreliste des übergebenen Schwierigkeitsgrads
     */
    public Set<HighscoreEntry> getHighscoreList(Level level) {
        loadHighscoreList();

        Set<HighscoreEntry> resultSet = null;
        switch (level) {
            case EASY:
                resultSet = easySet;
                break;
            case MIDDLE:
                resultSet = middleSet;
                break;
            case HARD:
                resultSet = hardSet;
                break;
        }
        return resultSet;
    }

    /**
     * speichert die aktuellen Highscores
     */
    private void saveHighscoreList() {
        try {
            File f = new File(saveDirectory);
            if (!f.exists()) {
                // falls die angegebenen Verzeichnisse nicht existieren, werden sie erstellt
                f.mkdirs();
            }
            FileOutputStream fileOut = new FileOutputStream(saveDirectory + saveFilename);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(easySet);
            out.writeObject(middleSet);
            out.writeObject(hardSet);
            out.close();
            fileOut.close();
            //System.out.println("Serialized data is saved in /saves/" + saveFilename + ".ser");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    /**
     * lädt die aktuellen Highscores
     */
    private void loadHighscoreList() {
        File f = new File(saveDirectory + saveFilename);

        if (f.exists()) {
            try {
                FileInputStream fileIn = new FileInputStream(f);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                easySet = (Set<HighscoreEntry>) in.readObject();
                middleSet = (Set<HighscoreEntry>) in.readObject();
                hardSet = (Set<HighscoreEntry>) in.readObject();

                in.close();
                fileIn.close();
            } catch (IOException i) {
                i.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * vor dem Testen von Highscore soll alte Liste gelöscht werden
     */
    public void deleteHighscorelist() {
        File f = new File(saveDirectory + saveFilename);
        if (f.exists())
            f.delete();
    }
}
