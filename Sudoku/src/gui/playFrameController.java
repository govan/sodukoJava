package gui;

import Model.Highscore;
import Model.SudokuModel;
import Model.TNumberField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller für playFrame
 * @author Maximilian Fröschl
 * @version 1.0
 * @since 21.06.2016
 */
public class playFrameController {
    public Button pruefenB;
    public BorderPane bPane;
    public MenuItem newGame;
    public MenuItem save;
    public MenuItem load;
    public MenuItem close;
    public Menu highscoreButton;
    public MenuItem openHighscore;
    public ToggleButton switchButton;

    private Stage stage;
    private Scene scene;
    private playFrame p;
    private SudokuModel sudokuModel;
    private TNumberField[][] sudokuField;

    private TextField tField = new TextField();

    /**
     * Setzt das SudokuModel
     * @param sModel    SudokuModel
     */
    public void setSudokuModel(SudokuModel sModel) {
        sudokuModel = sModel;
    }

    /**
     * Setzt die vorherige Szene als aktuelle Szene
     * @param scene vorherige Szene
     */
    public void setPrevScene(Scene scene) {
        this.scene = scene;
    }

    /**
     * Methode setzt eine neue Stage
     * @param stage neue Stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Überprüft das aktuelle Sudoku auf korrektheit
     * @param actionEvent ActionEvent, das die Methode ausgelöst hat
     */
    public void pruefenReact(ActionEvent actionEvent) {
        Highscore highscore;
        long playTimeInSeconds;
        Alert alert;

        if (sudokuModel.checkResult()) {
            highscore = new Highscore();
            // Spielzeit berechnen und abspeichern
            playTimeInSeconds = (System.currentTimeMillis() - sudokuModel.getStartTime().getTime())/1000;

            //System.out.println(playTimeInSeconds);
            //System.out.println(sudokuModel.getStartTime().getTime());

            sudokuModel.setPlayTimeInSec(playTimeInSeconds);
            // neuen Highscore-Eintrag hinzufügen
            highscore.createEntry(playTimeInSeconds, sudokuModel.getPlayerName(), sudokuModel.getLevel());
            // Erfolgsmeldung ausgeben
            alert = new Alert(Alert.AlertType.NONE, "Glückwunsch!\nDu hast es geschafft!", ButtonType.OK);
        } else {
            alert =  new Alert(Alert.AlertType.NONE, "Leider sind nicht alle Felder korrekt!\nBitte überprüfe deine Eingaben!", ButtonType.OK);
        }
        initContent();
        alert.showAndWait();
    }


    /**
     * Fügt das Sudoku-Feld hinzu und füllt es mit Inhalt
     */
    public void initContent() {


        p = new playFrame(sudokuModel);
        bPane.setCenter(p.getPlayFrame());


     /*   if (sudokuModel != null) {
           sudokuField= sudokuModel.getField();
           for (int a=0;a<3;a++){
               int x=0;
               int y=0;
               for (int b=0;b<3;b++){
                   for (int c=0;c<3;c++){
                       p.setLabels(x, b, sudokuField[a][y].getValue());
                       y++;
                   }
               }
               x++;
               y=0;
           }
       }*/
        sudokuField = sudokuModel.getField();
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                p.setLabels(x, y, sudokuField[x][y]);
            }
        }
    }

    /**
     * Startet ein neues Spiel
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     */
    public void newGameClick(ActionEvent actionEvent) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("schwierigkeitsgrad.fxml"));

        Parent root = null;
        try {
            root = (Parent) fxmlLoader.load();
            stage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
        scene = stage.getScene();


        schwierigkeitsgradController controller = fxmlLoader.<schwierigkeitsgradController>getController();
        controller.setStage(stage);
        //controller.setPrevScene(scene);
        stage.show();
    }

    /**
     * Speichert das aktuelle Spiel
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     */
    public void saveClick(ActionEvent actionEvent) {
        Alert alert;
        String saveGameName = sudokuModel.getPlayerName() + sudokuModel.getLevel().toString();
        if (sudokuModel.saveGame(saveGameName)) {
            alert = new Alert(Alert.AlertType.NONE, "Spiel \"" + saveGameName + "\" wurde erfolgreich gespeichert!", ButtonType.OK);
        } else {
            alert = new Alert(Alert.AlertType.NONE, "Leider ist beim Speichern ein Fehler aufgetreten!", ButtonType.OK);
        }
        alert.showAndWait();
    }

    /**
     * Läd die savegame Stage
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     * @throws IOException  wird ausgelöst, wenn FXMLLoader nicht laden konnte
     */
    public void loadClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("savegame.fxml"));

        Parent root = (Parent) fxmlLoader.load();
        scene = stage.getScene();

        stage.setScene(new Scene(root));
        savegameController controller = fxmlLoader.<savegameController>getController();
        controller.setStage(stage);
        controller.setPrevScene(scene);
        stage.show();
    }

    /**
     * Beendet die Application
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     */
    public void closeClick(ActionEvent actionEvent) {
        stage.close();
    }

    /**
     *Läd die highscore Stage
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     * @throws IOException  wird ausgelöst, wenn FXMLLoader nicht laden konnte
     */
    public void openHighscoreClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("highscore.fxml"));
        scene = stage.getScene();
        Parent root = (Parent) fxmlLoader.load();
        stage.setScene(new Scene(root));
        highscoreController controller = fxmlLoader.<highscoreController>getController();
        controller.setStage(stage);
        controller.setPrevScene(scene);
        stage.show();
    }
    /**
     * wechselt den Zustand der Notizen
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     */
    public void toggleSwitch(ActionEvent actionEvent) {
        if (p.getNotes()!=false){
            p.setNotes(false);
        }else {
            p.setNotes(true);
        }
    }
}

