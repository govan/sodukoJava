package gui;

import Model.Highscore;
import Model.HighscoreEntry;
import Model.Level;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Formatter;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Controller für die Highscoreliste
 *
 * @author Maximilian Fröschl
 * @version 1
 * @since 23.06.16
 */
public class highscoreController {
    public GridPane gridLeicht;
    public GridPane gridMittel;
    public GridPane gridSchwer;

    public Button back;

    private Stage stage;
    private Scene prevScene;
    private final int MAX_ENTRIES = 10;

    /**
     * Speichern der vorher angezeigten Scene, damit man ggf. wieder zurück kommt
     * @param scene Scene des vorhin angezeigten Fensters
     */
    public void setPrevScene(Scene scene){
        this.prevScene = scene;
    }

    /**
     * Speichern der Stage, damit man zwischen den Scenes wechseln kann
     * @param stage Stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Event-Handler für zurück-Button
     * @param actionEvent   ausgelöstes Ereignis
     */
    public void backClick(ActionEvent actionEvent) {
        stage.setScene(prevScene);
        stage.show();
    }

    /**
     * Methode, die nach Erstellung des Controllers aufgerufen wird
     * zeigt die Highscorelisten für die verschiedenen Schwierigkeitsgrade an
     */
    @FXML
    public void initialize() {
        int i, rank;
        long min, sec;
        StringBuilder s;
        Formatter f;
        Highscore highscore = new Highscore();
        final String TIME_FORMAT = "%d:%02d";
        final String TIME_INFO = "Zeit:";

        Set<HighscoreEntry> easySet = highscore.getHighscoreList(Level.EASY);
        Set<HighscoreEntry> middleSet = highscore.getHighscoreList(Level.MIDDLE);
        Set<HighscoreEntry> hardSet = highscore.getHighscoreList(Level.HARD);

        Iterator<HighscoreEntry> easyIterator = easySet.iterator();
        Iterator<HighscoreEntry> middleIterator = middleSet.iterator();
        Iterator<HighscoreEntry> hardIterator = hardSet.iterator();

        rank = 1;
        while (easyIterator.hasNext() && (rank <= MAX_ENTRIES)) {
            HighscoreEntry entry = easyIterator.next();
            min = entry.getPlayTimeInSeconds() / 60;
            sec = entry.getPlayTimeInSeconds() % 60;
            s = new StringBuilder();
            f = new Formatter();

            s.append(f.format(TIME_FORMAT, min, sec));

            gridLeicht.add(new Label(new Integer(rank).toString()), 0, rank - 1);
            gridLeicht.add(new Label(entry.getPlayerName()), 1, rank - 1);
            gridLeicht.add(new Label(TIME_INFO), 2, rank - 1);
            gridLeicht.add(new Label(s.toString()), 3, rank - 1);

            rank++;
        }

        rank = 1;
        while (middleIterator.hasNext() && (rank <= MAX_ENTRIES)) {
            HighscoreEntry entry = middleIterator.next();
            min = entry.getPlayTimeInSeconds() / 60;
            sec = entry.getPlayTimeInSeconds() % 60;
            s = new StringBuilder();
            f = new Formatter();

            s.append(f.format(TIME_FORMAT, min, sec));

            gridMittel.add(new Label(new Integer(rank).toString()), 0, rank - 1);
            gridMittel.add(new Label(entry.getPlayerName()), 1, rank - 1);
            gridMittel.add(new Label(TIME_INFO), 2, rank - 1);
            gridMittel.add(new Label(s.toString()), 3, rank - 1);

            rank++;
        }

        rank = 1;
        while (hardIterator.hasNext() && (rank <= MAX_ENTRIES)) {
            HighscoreEntry entry = hardIterator.next();
            min = entry.getPlayTimeInSeconds() / 60;
            sec = entry.getPlayTimeInSeconds() % 60;
            s = new StringBuilder();
            f = new Formatter();

            s.append(f.format(TIME_FORMAT, min, sec));

            gridSchwer.add(new Label(new Integer(rank).toString()), 0, rank - 1);
            gridSchwer.add(new Label(entry.getPlayerName()), 1, rank - 1);
            gridSchwer.add(new Label(TIME_INFO), 2, rank - 1);
            gridSchwer.add(new Label(s.toString()), 3, rank - 1);

            rank++;
        }

    }
}
