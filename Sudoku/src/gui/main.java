package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main Klasse
 * @author Maximilian Fröschl
 * @version 1.0
 * @since 21.06.2016
 */
public class main extends Application {

    /**
     * Main Methode
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    /**
     * Startet sie Javafx Application
     */
    public void start(Stage primaryStage) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("startseite.fxml"));
        Parent root= (Parent)fxmlLoader.load();
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Sudoku");
        startseiteController controller = fxmlLoader.<startseiteController>getController();
        controller.setStage(primaryStage);
        primaryStage.show();
    }
}
