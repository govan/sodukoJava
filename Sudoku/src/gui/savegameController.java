package gui;

import Model.HighscoreEntry;
import Model.Level;
import Model.SudokuModel;
import Model.SudokuSave;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller von savegame
 * @author Maximilian Fröschl
 * @version 1.0
 * @since 21.06.2016
 */
public class savegameController {
    public GridPane gridLeicht;
    public GridPane gridMittel;
    public GridPane gridSchwer;
    public Button back;
    public Tab tabSchwer;
    public Tab tabMittel;
    public Tab tabLeicht;
    public TabPane tabs;

    private Stage stage;
    private SudokuSave save = new SudokuSave();
    private SudokuModel sudokuModel;
    private Scene prevScene;

    /**
     * setzt das zu Speichernde Model
     * @param model
     */
    public void setModel(SudokuModel model) {
        this.sudokuModel = model;
    }

    /**
     * Speichert die vorherige Szene
     * @param scene
     */
    public void setPrevScene(Scene scene) {
        this.prevScene = scene;
    }

    /**
     * laedt eine Spiel mit dem uebergebenen Namen
     *
     * @param saveName
     */

    public void loadSave(String saveName) {
        SudokuModel model = new SudokuModel();

        sudokuModel = save.loadGame(model, saveName);

        FXMLLoader fxmlLoader = new FXMLLoader(playFrameController.class.getResource("playFrame.fxml"));

        Parent root;
        try {
            root = fxmlLoader.load();
            stage.setScene(new Scene(root, 600, 600));
        } catch (IOException e) {
            e.printStackTrace();
        }

        playFrameController controller = fxmlLoader.<playFrameController>getController();
        controller.setStage(stage);
        controller.setSudokuModel(sudokuModel);
        controller.initContent();
        stage.show();

    }


    /**
     * Gibt eine List<Path> zurück aus dem Pfad /saves
     * man kann in der liste mit:
     * list.get(0).toString(); den Pfad des Eintrages bekommen
     * result: savegame.ser
     * ungeordnet
     *
     * @return
     */

    public List<Path> showSaves() {
        List<Path> list = null;

        try {
            Path path = Paths.get("saves");
            list = save.showSaves(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    /**
     * Speichert die Stage
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }


    /**
     * Wechselt auf die vorherige Stage
     * @param actionEvent
     */
    public void backClick(ActionEvent actionEvent) {
        stage.setScene(prevScene);
        stage.show();
    }

    @FXML
    /**
     * wird ausgeführt sobald die FXML geladen wird
     */
    public void initialize() {
        int iEasy = 0;
        int iMiddle = 0;
        int iHard = 0;
        List<Path> list = showSaves();
        Iterator<Path> listIterator;

        if (list != null) {
            listIterator = list.iterator();

            while (listIterator.hasNext()) {
                String saveFilename = listIterator.next().toString();
                String saveName = saveFilename.substring(0, saveFilename.length() - 4); //-4 um das ".ser" abzuschneiden

                Label l = new Label(saveName);
                Button b = new Button("load");

                String finalSaveName = saveName;
                b.setOnAction(e -> {loadSave(finalSaveName);});

                // Spielstände nach Level aufteilen
                if (saveName.endsWith(Level.EASY.toString())) {
                    gridLeicht.add(l, 1, iEasy);
                    gridLeicht.add(b, 2, iEasy);
                    iEasy++;
                } else if (saveName.endsWith(Level.MIDDLE.toString())) {
                    gridMittel.add(l, 1, iMiddle);
                    gridMittel.add(b, 2, iMiddle);
                    iMiddle++;
                } else if (saveName.endsWith(Level.HARD.toString())) {
                    gridSchwer.add(l, 1, iHard);
                    gridSchwer.add(b, 2, iHard);
                    iHard++;
                }
            }
        }
    }
}
