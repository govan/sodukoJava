package gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller für die Startseite
 * @author Maximilian Fröschl
 * @version 1.0
 * @since 21.06.2016
 */
public class startseiteController {
    public Button neuesSpiel;
    public Button load;
    public Button highscore;
    public Button beenden;
    private Stage stage;
    private Scene scene;

    /**
     * Setzt die Stage auf eine neue Stage
     * @param stage neue Stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Läd die schwierigkeitsgrad Stage
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     * @throws IOException  wird ausgelöst, wenn FXMLLoader nicht laden konnte
     */
    public void neuesSpielClick(ActionEvent actionEvent) throws IOException {

        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource("schwierigkeitsgrad.fxml"));

        Parent root= (Parent)fxmlLoader.load();
        stage.setScene(new Scene(root));
        schwierigkeitsgradController controller = fxmlLoader.<schwierigkeitsgradController>getController();
        controller.setStage(stage);
        stage.show();
        /*

        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource("playFrame.fxml"));

        Parent root= (Parent)fxmlLoader.load();
        stage.setScene(new Scene(root, 600,600));
        playFrameController controller = fxmlLoader.<playFrameController>getController();
        controller.setStage(stage);
        controller.gitterHinzufuegen();
        stage.show();
        */
    }

    /**
     * Läd die savegame Stage
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     * @throws IOException  wird ausgelöst, wenn FXMLLoader nicht laden konnte
     */
    public void loadClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource("savegame.fxml"));

        Parent root= (Parent)fxmlLoader.load();
        scene =stage.getScene();

        stage.setScene(new Scene(root));
        savegameController controller = fxmlLoader.<savegameController>getController();
        controller.setStage(stage);
        controller.setPrevScene(scene);
        stage.show();
    }

    /**
     * Läd die Highscore Stage
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     * @throws IOException  wird ausgelöst, wenn FXMLLoader nicht laden konnte
     */
    public void highscoreClick(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader= new FXMLLoader(getClass().getResource("highscore.fxml"));
        scene=stage.getScene();
        Parent root= (Parent)fxmlLoader.load();
        stage.setScene(new Scene(root));
        highscoreController controller = fxmlLoader.<highscoreController>getController();
        controller.setStage(stage);
        controller.setPrevScene(scene);
        stage.show();
    }

    /**
     * Beendet die Application
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     */
    public void beendenClick(ActionEvent actionEvent) {
        stage.close();
    }
}
