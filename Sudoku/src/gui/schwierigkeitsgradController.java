package gui;

import Model.Level;
import Model.SudokuModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller für schwierigkeitsgrad
 * @author Maximilian Fröschl
 * @version 1.0
 * @since 21.06.2016
 */
public class schwierigkeitsgradController {
    public RadioButton leicht;
    public RadioButton mittel;
    public RadioButton schwer;
    public TextField tField;
    public Button start;

    private Stage stage;
    private ToggleGroup group = new ToggleGroup();
    private String name;
    private SudokuModel sudokuModel = new SudokuModel();
    private Level level;

    /**
     * Setzt die Stage auf eine neue Stage
     * @param stage neue Stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }


    /**
     * Startet ein neues Spiel
     * @param actionEvent   ActionEvent, das die Methode ausgelöst hat
     * @throws IOException  wird ausgelöst, wenn FXMLLoader nicht laden konnte
     */
    public void startClick(ActionEvent actionEvent) throws IOException {
        name = tField.getText();

        // prüfen, ob Eingaben vollständig sind
        if (name.equals("") || (group.getSelectedToggle() == null)) {
            Alert alert = new Alert(Alert.AlertType.NONE, "Bitte vollständige Eingaben machen!", ButtonType.OK);
            alert.showAndWait();
            return;
        }

        if (group.getSelectedToggle().equals(leicht)) {
            level = Level.EASY;
        }
        if (group.getSelectedToggle().equals(mittel)) {
            level = Level.MIDDLE;
        }
        if (group.getSelectedToggle().equals(schwer)) {
            level = Level.HARD;
        }


        sudokuModel.newGame(name, level);


        FXMLLoader fxmlLoader = new FXMLLoader(playFrameController.class.getResource("playFrame.fxml"));

        Parent root = (Parent) fxmlLoader.load();
        stage.setScene(new Scene(root, 600, 600));
        playFrameController controller = fxmlLoader.<playFrameController>getController();
        controller.setStage(stage);
        controller.setSudokuModel(sudokuModel);
        controller.initContent();
        stage.show();

    }

    /**
     * wird ausgeführt sobald die FXML geladen wird
     */
    @FXML
    public void initialize() {
        leicht.setToggleGroup(group);
        mittel.setToggleGroup(group);
        schwer.setToggleGroup(group);

        leicht.setSelected(true);
        // start.setDisable(true);
    }
}
