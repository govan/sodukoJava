package gui;

import Model.FieldInfo;
import Model.SudokuModel;
import Model.TNumberField;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

/**
 * Klasse implementiert das Sudoku-Feld als GUI
 * @author Maximilian Fröschl
 * @version 1.0
 * @since 21.06.2016
 */
public class playFrame {
    private Scene s;
    private Label[][] lArray;
    private GridPane[][] lowerG = new GridPane[3][3];
    private TNumberField[][] field;
    private SudokuModel model;
    private TextField tField;
    private Group g;
    private boolean notes=false;


    private GridPane mainG = new GridPane();

    /**
     * Konstruktor, initalisiert Variablen
     * @param model Aktuelles Model zur weiterverarbeitung
     */
    public playFrame(SudokuModel model) {
        tField = new TextField();
        tField.setVisible(false);
        this.model = model;
        lArray = new Label[9][9];
    }

    /**
     * Initialisiert das Spielfeld mit allen 81 Labels
     */
    private void init() {

        field=model.getField();


        mainG.setGridLinesVisible(true);
        mainG.setVgap(5);
        mainG.setHgap(5);


        g = new Group(mainG, tField);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                lowerG[i][j] = new GridPane();
                lowerG[i][j].setGridLinesVisible(true);
                mainG.add(lowerG[i][j], i, j);
            }
        }

        int tmp = 3;
        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                for (int c = 0; c < 3; c++) {
                    for (int d = 0; d < 3; d++) {
                        lArray[b + a * tmp][d + c * tmp] = new Label("*" + "*");

                        lArray[b + a * tmp][d + c * tmp].setMinSize(50, 50);

                        lowerG[a][c].add(lArray[b + a * tmp][d + c * tmp], b, d);
                    }
                }
            }
        }
    }

    /**
     * Giebt das generierte Sudoku-Feld wieder
     * @return Group bestehend aus GridPane und Textfield
     */
    public Group getPlayFrame() {
        init();
        return g;
    }

    /**
     * Methode zum Setzen der Labels und ActionEvents
     * @param x X-Koordinate in Array
     * @param y Y-Koordinate im Array
     * @param content entsprechendes TNumberfield
     */
    public void setLabels(int x, int y, TNumberField content) {

        String font = "";
        String background = "";
        field[x][y]=content;

        if (content.getInfo() == FieldInfo.GIVEN) {
            lArray[x][y].setText("" + content.getValue());
            font = "-fx-font: 24 arial;";
        } else  {
            if((content.getInfo() == FieldInfo.WRONG) && (content.getValue() != 0)){
                background = "-fx-background-color: red";
            }
            if (content.getValue() == 0) {

                if (content.getNote1()!=0 && content.getNote2()==0){
                    lArray[x][y].setText(content.getNote1()+"");
                    lArray[x][y].setTextFill(Color.BLUE);
                }
                else {
                    if (content.getNote2()!=0){
                        lArray[x][y].setText(content.getNote1()+"|"+content.getNote2());
                        lArray[x][y].setTextFill(Color.BLUE);
                    }
                }

                if (content.getNote1()==0 && content.getNote2()==0){
                lArray[x][y].setText("");}
            } else {
                lArray[x][y].setText("" + content.getValue());
            }
           /* if (content.getNote1()!=0 && content.getNote2()==0){
                lArray[x][y].setText(content.getNote1()+"");
                lArray[x][y].setTextFill(Color.BLUE);
            }
            else {
                if (content.getNote2()!=0){
                    lArray[x][y].setText(content.getNote1()+"|"+content.getNote2());
                    lArray[x][y].setTextFill(Color.BLUE);
                }
            }
            */

            lArray[x][y].setOnMouseClicked(event -> {
                tField.setMaxSize(50, 50);
                tField.setMinSize(50, 50);

                int aMod = 0;
                int bMod = 0;

                if (x / 3 >= 1) {
                    aMod = 1;
                }

                if (y / 3 >= 1) {
                    bMod = 1;
                }

                if (x / 3 >= 2) {
                    aMod = 2;
                }

                if (y / 3 >= 2) {
                    bMod = 2;
                }
                
                tField.setLayoutX(x * 50 + 5 * (aMod));
                tField.setLayoutY(y * 50 + 5 * (bMod));


                //tField.setLayoutX();
                //tField.setLayoutY();
                tField.setStyle("-fx-background-color: yellow;");
                tField.setVisible(true);
                tField.requestFocus();

                    tField.setOnKeyReleased(new EventHandler<KeyEvent>() {
                    @Override
                    public void handle(KeyEvent ke) {
                        String sNumber;
                        int number;

                        if (ke.getCode().isDigitKey()) {
                            sNumber = tField.getText();
                            number = Integer.parseInt(sNumber);
                            if ((number > 0) && (number < 10)){
                                if (!notes) {
                                    lArray[x][y].setTextFill(Color.BLACK);
                                    lArray[x][y].setText(sNumber);
                                    field[x][y].setValue(number);
                                    model.setNumberField(x, y, field[x][y]);
                                }
                                if (notes){
                                    if (field[x][y].getNote1()==0){
                                        field[x][y].setNote1(number);
                                        lArray[x][y].setText(field[x][y].getNote1()+"");

                                        model.setNumberField(x, y, field[x][y]);
                                        lArray[x][y].setTextFill(Color.BLUE);
                                    }
                                    else {
                                        if (field[x][y].getNote2()==0){
                                            field[x][y].setNote2(number);
                                            lArray[x][y].setText(field[x][y].getNote1()+"|"+field[x][y].getNote2());
                                            model.setNumberField(x, y, field[x][y]);
                                            lArray[x][y].setTextFill(Color.BLUE);
                                        }
                                    }
                                }
                            }


                            //field[a][b].setValue(Integer.parseInt(tField.getText()));
                            tField.setText("");
                            tField.setVisible(false);

                        }
                    }
                });
            });
        }
        String alignmentCenter = "-fx-alignment: center;";
        lArray[x][y].setStyle(alignmentCenter+font+background);
        //System.out.println(lArray[a][b].getStyle());

    }

    /**
     * Methode zum Abfragen, ob Notizen aktiviert sind
     * @return  TRUE    Notizen sind aktiviert
     *          FALSE   Notizen sind deaktiviert
     */
    public boolean getNotes(){return notes;}

    /**
     * Methode aktiviert bzw. deaktiviert Notizen
     * @param n TRUE    aktiviert Notizen
     *          FALSE   deaktiviert Notizen
     */
    public void setNotes(boolean n){notes=n;}


}

