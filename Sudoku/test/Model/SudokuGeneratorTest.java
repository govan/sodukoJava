package Model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by Vanessa on 21.06.2016.
 * */
 public class SudokuGeneratorTest {

    @Test
    public void setField() throws Exception {
        TNumberField[][] field = new TNumberField[9][9];
        SudokuGenerator gen = new SudokuGenerator();
        gen.setWholeField(field);
        gen.setField(4, 0, 0);
        assertEquals(4,gen.getField(0,0).getValue());
    }


    @Test
    public void generateStandardSudoku() throws Exception {
        SudokuGenerator gen = new SudokuGenerator();
        gen.generateStandardSudoku(Level.EASY);
        for (int y2 = 0; y2 < 9; y2++) {
            for (int x2 = 0; x2 < 9; x2++) {
                
                System.out.print(gen.getField(x2,y2).getValue()+" ");
            }
            System.out.println();
        }
        gen.getField();
    }
}