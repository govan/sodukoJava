package Model;

import Model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Florian on 18.06.2016.
 */
public class SudokuModelTest {

    @Test
    public void newGame() throws Exception {
        SudokuModel sudokuModel = new SudokuModel();
        assertTrue(sudokuModel.newGame("Theo Tester", Level.EASY));
        assertNotNull(sudokuModel.getField());
    }

    @Test
    public void checkResult() throws Exception {
        SudokuModel sudokuModel = new SudokuModel();
        SudokuGenerator sudokuGenerator = new SudokuGenerator();
        TNumberField[][] field;
        TNumberField[][] fieldFull;

        if (sudokuGenerator.generateStandardSudoku(Level.HARD)) {
            field = sudokuGenerator.getField();
            fieldFull = sudokuGenerator.getFieldFull();

            sudokuModel.setField(fieldFull, fieldFull);
            assertTrue(sudokuModel.checkResult());

            sudokuModel.setField(field, fieldFull);
            assertFalse(sudokuModel.checkResult());
        } else {
            fail("Fehler beim Generieren des Feldes");
        }
    }

    @Test
    public void setNumberField() throws Exception {
        SudokuModel sudokuModel = new SudokuModel();
        SudokuGenerator sudokuGenerator = new SudokuGenerator();
        int row = 0, col = 0;
        int oldVal, newVal;

        if (sudokuGenerator.generateStandardSudoku(Level.HARD)) {
            sudokuModel.setField(sudokuGenerator.getField(), sudokuGenerator.getFieldFull());
            oldVal = sudokuModel.getField()[row][col].getValue();
            // wenn alter Wert ungleich 9 ist, wird 1 dazu addiert, ansonsten wird 1 zurück gegeben
            newVal = (oldVal != 9) ? oldVal + 1 : 1;
            sudokuModel.setNumberField(row, col, new TNumberField(newVal));
            assertEquals(newVal, sudokuModel.getField()[row][col].getValue());
            assertEquals(FieldInfo.NORMAL, sudokuModel.getField()[row][col].getInfo());
        } else {
            fail("Fehler beim Generieren des Feldes");
        }
    }
}