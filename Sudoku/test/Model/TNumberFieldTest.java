package Model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Florian on 21.06.2016.
 */
public class TNumberFieldTest {

    @Test
    public void equals_hashCode_contract() throws Exception {
        TNumberField field1 = new TNumberField(5);
        TNumberField field2 = new TNumberField(6);
        TNumberField field3 = new TNumberField(5, FieldInfo.WRONG);
        TNumberField field4 = new TNumberField(5);

        assertFalse(
                field1.equals(field2) ||
                field1.equals(field3) ||
                field2.equals(field3)
        );
        assertTrue(field1.equals(field4));
        assertFalse(
                (field1.hashCode() == field2.hashCode()) ||
                (field1.hashCode() == field3.hashCode()) ||
                (field2.hashCode() == field3.hashCode())
        );
        assertTrue(field1.hashCode() == field4.hashCode());
    }
}