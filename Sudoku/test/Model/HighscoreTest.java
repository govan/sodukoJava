package Model;

import org.junit.Test;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by Florian on 17.06.2016.
 */
public class HighscoreTest {

    @Test
    public void createEntry() throws Exception {
        Highscore highscore = new Highscore();
        Iterator<HighscoreEntry> easyIterator;
        HighscoreEntry entry;
        int i;

        // falls schon eine Liste vorhanden, wird diese gelöscht
        highscore.deleteHighscorelist();

        highscore.createEntry(300, "Herbert", Level.EASY);
        highscore.createEntry(50, "Hubert", Level.EASY);
        highscore.createEntry(50, "Norbert", Level.EASY);
        highscore.createEntry(51, "Norbert", Level.EASY);

        highscore.createEntry(30, "Herbert", Level.MIDDLE);
        highscore.createEntry(50, "Hubert", Level.MIDDLE);

        easyIterator = highscore.getHighscoreList(Level.EASY).iterator();
        i = 0;
        while (easyIterator.hasNext()) {
            entry = easyIterator.next();
            if (i == 0)
                assertEquals("Hubert", entry.getPlayerName());
            if (i == 1)
                assertEquals("Norbert", entry.getPlayerName());
            if (i == 2)
                assertEquals("Norbert", entry.getPlayerName());
            if (i == 3)
                assertEquals("Herbert", entry.getPlayerName());
            i++;
        }
    }

    @Test
    public void testDeleteHighscorelist() throws Exception {
        Highscore highscore;
        File f = new File("saves/highscore/Highscore.ser");
        highscore = new Highscore();
        if (f.exists()) {
            highscore.deleteHighscorelist();
            assertFalse(f.exists());
        }
    }
}