package Model;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Florian on 16.06.2016.
 */
public class SudokuSaveTest {
    @Test
    public void showSaves() throws Exception {
        SudokuSave save = new SudokuSave();
        SudokuModel model = new SudokuModel();

        model.saveGame("CheckList");

        List<Path> list = new ArrayList<>();
        try {
            Path path = Paths.get("saves");
            list = save.showSaves(path);
        } catch (IOException e) {
            System.out.println("File not found!");
            e.printStackTrace();
        }

        assertEquals("CheckList.ser", list.get(0).toString());


        //System.out.println("Working Directory = " + System.getProperty("user.dir"));
        //System.out.println(path1.getFileName().toString());
        // Path path = Paths.get("/src/saves/");
    }

    @Test
    public void saveGame() throws Exception {
        SudokuModel model = new SudokuModel();

        assertTrue(model.saveGame("testSave"));
    }

    @Test
    public void loadGame() {
        SudokuModel model = new SudokuModel();
        SudokuModel model2 = new SudokuModel();

        TNumberField[][] field = new TNumberField[9][9];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                field[i][j] = new TNumberField((int) (Math.random() * 9));
            }
        }

        model.setField(field, field);
        model2.setField(field, field);


        SudokuSave save = new SudokuSave();
        model.saveGame("loadTestSave");

        model2 = save.loadGame(model, "loadTestSave");

        assertArrayEquals(model2.getField(), model.getField());
    }

}